"use strict";

const comments = document.querySelectorAll('.what-people-say__comment');
const icons = document.querySelector('.what-people-say__icons');
const chooseBtns = document.querySelectorAll('.choose__btn');
let currentAuthorIconIndex = 0;

const handleCurrentIcon = (index) => {
	for (let i = index; i < icons.children.length; i++) {
		if (icons.children[i].classList.contains('what-people-say__icon--current')) {
			icons.children[i].style.bottom = "15px";
			currentAuthorIconIndex = i;
		}
		else {
			icons.children[i].style.bottom = "";
		}
	}
}

const handleCurrentComment = () => {
	const currentAuthorIcon = document.querySelector('.what-people-say__icon--current');

	comments.forEach((comment, index) => {
		comment.style.visibility = 'hidden';
		comment.style.opacity = '0';

		handleCurrentIcon(index);

		if (currentAuthorIcon.dataset.commentAuthor === comment.dataset.commentAuthor) {
			comment.style.visibility = 'visible';
			comment.style.opacity = '1';
		}
	});
}

handleCurrentComment();

icons.addEventListener('click', (e) => {
	if (e.target.tagName === "IMG") {
		e.currentTarget.childNodes.forEach(icon => {
			icon.classList?.remove('what-people-say__icon--current');
		});
		e.target.classList.add("what-people-say__icon--current");

		handleCurrentComment();
	}
});

chooseBtns.forEach(chooseBtn => {
	chooseBtn.addEventListener('click', (e) => {
		const icon = icons.children;
		const maxIcons = icon.length - 1;

		let nextIcon = currentAuthorIconIndex + 1;
		let prevIcon = currentAuthorIconIndex - 1;

		if (currentAuthorIconIndex === maxIcons) {
			nextIcon = 0;
		}
		if (currentAuthorIconIndex === 0) {
			prevIcon = maxIcons;
		}

		if (e.currentTarget.classList.contains('choose__btn--next')) {
			icon[currentAuthorIconIndex].classList.remove('what-people-say__icon--current');
			icon[nextIcon].classList.add('what-people-say__icon--current');
			handleCurrentIcon(nextIcon);
		}
		else {
			icon[currentAuthorIconIndex].classList.remove('what-people-say__icon--current');
			icon[prevIcon].classList.add('what-people-say__icon--current');
			handleCurrentIcon(prevIcon);
		}

		handleCurrentComment();
	});
});