"use strict";

const tabsNav = document.querySelector('.works__tabs-nav');
const worksHolder = document.querySelector('.works__holder');
const loadMoreBtn = document.querySelector('.works .btn--load-more');
let activeTab = document.querySelector('.works__tab-title--active');

const workCardCollection = worksHolder.querySelectorAll('.work__card');

// Додає до зображень alt та назву категорії у картці
workCardCollection.forEach(card => {
	const cardCategory = card.querySelector('.work__card-category');
	const cardImg = card.querySelector('.work__img');
	cardCategory.innerText = card.dataset.cardCategory;
	cardImg.alt = card.dataset.cardCategory;
})

let currentCategory = "all";
let currentCards = 12;
let cardsDisplayed = 0;

const HandleWorksTabs = () => {
	for (let i = 0; i < currentCards; i++) {
		workCardCollection[i].style.display = "block";
	}

	tabsNav.addEventListener('click', (e) => {
		currentCategory = e.target.dataset.tabTitle;

		e.currentTarget.childNodes.forEach(tab => {
			tab.classList?.remove('works__tab-title--active');
		});
		if (e.target.tagName === "LI") {
			e.target.classList.add('works__tab-title--active');
			activeTab = document.querySelector('.works__tab-title--active');
		}

		cardsDisplayed = 0;

		if (currentCategory === "all") {
			workCardCollection.forEach((card) => {
				card.style.display = "none";
				if (cardsDisplayed < 12) {
					card.style.display = "block";
					cardsDisplayed++;
					currentCards = 12;
				}
			});
			worksHolder.parentElement.append(loadMoreBtn);
		}
		else {
			workCardCollection.forEach((card) => {
				card.style.display = "none";
				if (card.dataset.cardCategory === currentCategory) {
					if (cardsDisplayed < 12) {
						card.style.display = "block";
						cardsDisplayed++;
						currentCards = 12;
					}
				}
			});
		}

		// Якщо карток менше 12-ти, кнопка "Завантажити більше" - зникає
		cardsDisplayed < 12 ? loadMoreBtn.remove() : worksHolder.parentElement.append(loadMoreBtn);

		console.log(cardsDisplayed);
	});
}

HandleWorksTabs();

let currentCategoryCardIndex;
loadMoreBtn.addEventListener('click', () => {
	loadMoreBtn.classList.add('btn--loading');
	const loadingState = setTimeout(() => {
		if (currentCategory === "all") {
			workCardCollection.forEach((card, index) => {
				if (index < currentCards + 12) {
					card.style.display = "block";
				}
			});
		}
		else {
			currentCategoryCardIndex = 0;
			workCardCollection.forEach((card) => {
				if (card.dataset.cardCategory === currentCategory) {
					if (currentCategoryCardIndex < currentCards + 12) {
						card.style.display = "block";
						currentCategoryCardIndex++;
					}
				}
			});

			// Якщо у категорії більше ніж 36 картки - видалити кнопку
			currentCategoryCardIndex >= 36 ? loadMoreBtn.remove() : '';
		}
		currentCards += 12;

		loadMoreBtn.classList.remove('btn--loading');

	}, 2000);

});