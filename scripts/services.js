"use strict";

const showServicesTab = () => {
	const tabsNav = document.querySelector('.services__tabs-nav');
	const ativeTab = document.querySelector('.services__tab-title--active');
	const tabsContent = document.querySelectorAll('.service__content');

	tabsContent.forEach(tab => {
		if (!(ativeTab.dataset.tabTitle === tab.dataset.tabContent)) {
			tab.style.display = "none";
		}
	})

	tabsNav.addEventListener('click', (e) => {
		e.currentTarget.childNodes.forEach(tab => tab.classList?.remove('services__tab-title--active'));
		e.target.classList.add('services__tab-title--active');

		tabsContent.forEach(tab => {
			tab.style.display = "none";
			if (e.target.dataset.tabTitle === tab.dataset.tabContent) {
				tab.style.display = "";
			}
		})
	})
}

showServicesTab();